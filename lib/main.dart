import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Login/FirstPageLogin.dart';
import 'package:ios_flutter/src/Modelo/Consulta.dart';
import 'package:ios_flutter/src/Modelo/Seguimiento.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:ios_flutter/src/firstPage.dart';
import 'package:ios_flutter/src/firstTimePage.dart';
import 'package:pref_dessert/pref_dessert.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_){
    runApp(MaterialApp(
      title: "DreamAnalyzer",
      routes: <String, WidgetBuilder>{
        '/': (context) => Intro(),
      },
      theme: ThemeData(

      ),
    ));
  });
} 

class Intro extends StatefulWidget {
  @override
  _Intro createState() => _Intro();
}

class _Intro extends State<Intro> {
  User _user = new User();

  existe() async {
    _user = new User();
    var repo = new FuturePreferencesRepository<User>(new UserDesSer());
    var repoCons =
        new FuturePreferencesRepository<Consulta>(new ConsultaDesSer());
    var repoSeg =
        new FuturePreferencesRepository<Seguimiento>(new SeguimientoDesSer());
    var auth = new FireBaseAuth();
    var listaUsuarios = await repo.findAll();
    var listaSeg = await repoSeg.findAll();
    var listaCons = await repoCons.findAll();
    if (listaUsuarios.isEmpty || !auth.isLoggedin()) {
      repoCons.removeAll();
      repoSeg.removeAll();
      repo.removeAll();
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => FirstTimeLogin(_user)));
    } else {
      _user = listaUsuarios[0];

      if (_user.skinToneImage == null) {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => FirstTimePage(_user)));
      }

      for (Seguimiento seg in listaSeg) {
        if (seg.name == null) {
          continue;
        }
        for (Consulta cons in listaCons) {
          if (cons.indicador != null && seg.name == cons.indicador) {
            seg.consultas.add(cons);
          }
        }
      }

      listaSeg.forEach((value) {
        if (value.name != null) {
          _user.addSeguimiento(value);
        }
      });

      Navigator.push(
          context, MaterialPageRoute(builder: (context) => FirstPage(_user)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color.fromARGB(255, 50, 86, 170),
        alignment: Alignment.center,
        child: GestureDetector(
          child: Hero(
            tag: "intro",
            child: Container(
              constraints: BoxConstraints(maxWidth: 400, maxHeight: 400),
              child: Image(image: new AssetImage("assets/logo.png"),)
            ),
          ),
          onTap: existe,
        ),
      ),
    );
  }
}

import 'package:firebase_auth/firebase_auth.dart';
import 'package:ios_flutter/src/Modelo/Consulta.dart';
import 'package:ios_flutter/src/Modelo/Seguimiento.dart';
import 'package:pref_dessert/pref_dessert.dart';
import 'package:ios_flutter/src/Modelo/User.dart';

class FireBaseAuth {
  FirebaseUser _user;
  
    bool isLoggedin() {
    return FirebaseAuth.instance.currentUser() != null ? true : false;
  }
  Future<FirebaseUser> getUser()async{
    return await FirebaseAuth.instance.currentUser();
  }

  Future<FirebaseUser> login(String email, String password) async {
    _user = await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: email, password: password);
    return _user;
  }

  Future<FirebaseUser> createUser(String email, String password) async {
    _user = await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password);
    return _user;
  }

  Future logOut() async {
    FirebaseAuth.instance.signOut();
    _user = null;
    var repo = new FuturePreferencesRepository<User>(new UserDesSer());
    var repo1 = new FuturePreferencesRepository<Consulta>(new ConsultaDesSer());
    var repo2 = new FuturePreferencesRepository<Seguimiento>(new SeguimientoDesSer());
    repo.removeAll();
    repo1.removeAll();
    repo2.removeAll();
  }
}

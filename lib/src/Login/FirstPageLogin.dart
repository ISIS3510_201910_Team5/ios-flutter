import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Login/Login.dart';
import 'package:ios_flutter/src/Login/SignIn.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:ios_flutter/src/texts.dart' as txts;

class FirstTimeLogin extends StatefulWidget {
  final Widget child;
  final User user;

  FirstTimeLogin(this.user, {Key key, this.child}) : super(key: key);

  _FirstTimeLoginState createState() => _FirstTimeLoginState(user);
}

class _FirstTimeLoginState extends State<FirstTimeLogin> {
  User _user;
  _FirstTimeLoginState(this._user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color.fromARGB(255, 50, 86, 170),
        child: ListView(
          children: <Widget>[
            Hero(
              tag: "intro",
              child: Container(
                constraints: BoxConstraints(maxWidth: 300, maxHeight: 300),
                child: Image(image: new AssetImage("assets/logo.png"),)
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 40, right: 40),
              child: Column(
                children: <Widget>[
                  Text(
                    'Leskan',
                    style: TextStyle(color: Colors.white, fontSize: 50),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  RaisedButton(
                    child: Text(
                      txts.inicaSesion,
                      style: TextStyle(color: Colors.white),
                    ),
                    color: Color.fromARGB(255, 27, 154, 247),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Login(_user)));
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  RaisedButton(
                    color: Color.fromARGB(255, 27, 154, 247),
                    child: Text(txts.registrare,
                        style: TextStyle(color: Colors.white)),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => SignIn()));
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

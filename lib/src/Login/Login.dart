import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Login/ResetPass.dart';
import 'package:ios_flutter/src/Modelo/Consulta.dart';
import 'package:ios_flutter/src/Modelo/Seguimiento.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:ios_flutter/src/Services/CrudSeguimiento.dart';
import 'package:toast/toast.dart';
import 'package:validate/validate.dart';
import 'package:ios_flutter/src/Login/errorMessage.dart';
import 'package:ios_flutter/src/Widgets/BuildTextFormField.dart';
import 'package:ios_flutter/src/firstTimePage.dart';
import 'package:ios_flutter/src/Services/CrudUser.dart';
import 'package:ios_flutter/src/firstPage.dart';
import 'package:pref_dessert/pref_dessert.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/texts.dart' as txts;

class Login extends StatefulWidget {
  final User user;

  Login(this.user);

  @override
  _Login createState() => _Login(user);
}

class _Login extends State<Login> {
  _Login(this._user);

  CrudMethodsSeguimiento _servicesSegimiento = new CrudMethodsSeguimiento();
  CrudMethodsUser _servicesUser = new CrudMethodsUser();

  User _user;

  final _formKey = GlobalKey<FormState>();

  String _email;

  String _password;

  bool inic = false;

  String _validateEmail(String value) {
    try {
      Validate.isEmail(value);
    } catch (e) {
      return txts.correoValido;
    }
    return null;
  }

  String _validatePassword(String value) {
    if (value.length < 8) {
      return txts.contraseniaInvalida;
    }
    return null;
  }

  void submit() async {
    // revisa si estas conectado a internet

    var estado = await Connectivity().checkConnectivity();
    if (estado == ConnectivityResult.none) {
      inic = false;
      showDialog(
        context: this.context,
        builder: (context) =>
            errorMessage(context, "No esta conectado a internet"),
      );
      return;
    }

    // First validate form.
    if (this._formKey.currentState.validate()) {
          inic = true;
      _formKey.currentState.save(); // Save our form now.

      // autenticar
      var auth = new FireBaseAuth();

      auth.login(_email, _password).then((value) {
        // obtener informacion basica
        _servicesUser.setUser(value.uid, _user).then((user) {
          _servicesSegimiento.getSeguimientos(value.uid, user).then((user2) {
            _user = user2;
            var repo = new FuturePreferencesRepository<User>(new UserDesSer());
            var repoSeg = new FuturePreferencesRepository<Seguimiento>(
                new SeguimientoDesSer());
            var repoCons =
                new FuturePreferencesRepository<Consulta>(new ConsultaDesSer());

            //persistir
            repo.removeAll();
            repo.save(_user);
            _user.seguimientos.forEach((seg) {
              repoSeg.save(seg);
              seg.consultas.forEach((con) {
                repoCons.save(con);
              });
            });

            // es primera vez?
            if (_user.skinToneImage == '') {
              inic = true;
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FirstTimePage(_user)));
            } else {
              inic = true;
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FirstPage(_user)));
            }
          });
        });
      }).catchError((error) {
        inic = false;

        Toast.show("Correo o contraseña invalida, intente nuevamente.", context,
            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color.fromARGB(255, 50, 86, 170),
        child: ListView(
          children: <Widget>[
            Hero(
              tag: "intro",
              child: Container(
                  constraints: BoxConstraints(maxWidth: 200, maxHeight: 200),
                  child: Image(
                    image: new AssetImage('assets/logo.png'),
                  )),
            ),
            Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        BuildTextFormField(null, _validateEmail, false,
                            'you@mail.com', 'E-mail', (String value) {
                          this._email = value;
                        }, context),
                        SizedBox(
                          height: 20,
                        ),
                        BuildTextFormField(null, _validatePassword, true,
                            'Contraseña', 'Contraseña', (String value) {
                          this._password = value;
                        }, context),
                        SizedBox(
                          height: 20,
                        ),
                        (!inic)
                            ? new RaisedButton(
                                color: Color.fromARGB(255, 27, 154, 247),
                                onPressed: () {
                                  if (!inic) {
                                    this.submit();
                                  }
                                },
                                child: Text(
                                  "Iniciar sesión",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            : CircularProgressIndicator(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              alignment: Alignment.center,
              child: GestureDetector(
                  child: Text(
                    'Olvidaste la Contraseña? Haz click acá.',
                    style: TextStyle(color: Colors.white),
                  ),
                  onTap: () async {
                    var estado = await Connectivity().checkConnectivity();
                    if (estado == ConnectivityResult.none) {
                      showDialog(
                        context: this.context,
                        builder: (context) => errorMessage(context,
                            "No esta conectado a internet, revise su conexión e intente nuevamente."),
                      );
                      return;
                    }
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ResetPassword()));
                  }),
            ),
          ],
        ),
      ),
    );
  }
}

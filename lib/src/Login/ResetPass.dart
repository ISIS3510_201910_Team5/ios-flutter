import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Login/errorMessage.dart';
import 'package:ios_flutter/src/Login/warningMessage.dart';
import 'package:ios_flutter/src/Widgets/BuildTextFormField.dart';
import 'package:validate/validate.dart';

class ResetPassword extends StatefulWidget {
  final Widget child;

  ResetPassword({Key key, this.child}) : super(key: key);

  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  final _formKey = GlobalKey<FormState>();

  String _email;

  String _validateEmail(String value) {
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'The E-mail Address must be a valid email address.';
    }
    return null;
  }

  void enviarEmail() {
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save();
      FirebaseAuth.instance.sendPasswordResetEmail(email: _email).then((_) {
        showDialog(
          context: this.context,
          builder: (context) => warningMessage(context,
              "Se envio un correo de verificacion. \n Revise su bandeja de entrada."),
        ).then((_) {
          Navigator.pop(context);
        });
      }).catchError((e) {
        showDialog(
          context: this.context,
          builder: (context) => errorMessage(context, e.message),
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color.fromARGB(255, 50, 86, 170),
        child: ListView(
          children: <Widget>[
            Hero(
              tag: "intro",
              child: Container(
                constraints: BoxConstraints(maxWidth: 200, maxHeight: 200),
                child: Image(image: new AssetImage("assets/logo.png"),)
              ),
            ),
            Text(
              "Restablecer Contraseña",
              style: TextStyle(
                color: Colors.white,
                fontSize: 30,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,
            ),
            Card(
              child: Container(
                padding: EdgeInsets.only(left: 40, right: 40),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      BuildTextFormField(null, _validateEmail, false,
                          "Correo Electronico", "you@mail.com", (value) {
                        setState(() {
                          _email = value;
                        });
                      }, context),
                      SizedBox(
                        height: 40,
                      ),
                      RaisedButton(
                        color: Color.fromARGB(255, 27, 154, 247),
                        onPressed: enviarEmail,
                        child: Text(
                          "Enviar Email",
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

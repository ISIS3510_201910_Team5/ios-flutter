import 'dart:async';
import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Services/CrudUser.dart';
import 'package:ios_flutter/src/Widgets/BuildTextFormField.dart';
import 'package:validate/validate.dart';
import 'package:ios_flutter/src/Login/approvedMessage.dart';
import 'package:ios_flutter/src/Login/errorMessage.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => new _SignInState();
}

class _SignInState extends State<SignIn> {
  final _formKey = GlobalKey<FormState>();

  String _email;

  String _password;

  String _nombre;

  String _apellido;

  int _sexo;

  bool _presiono = false;

  CrudMethodsUser _services = new CrudMethodsUser();

  DateTime _birthDay = new DateTime(DateTime.now().year-18,DateTime.now().month,DateTime.now().day);

  ///controller para el texto de la contrasenia 2
  TextEditingController _textController = new TextEditingController();

  /// valida el campo del Email
  String _validateEmail(String value) {
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'The E-mail Address must be a valid email address.';
    }
    return null;
  }

  /// Valida el campo del Password
  String _validatePassword(String value) {
    if (value.length < 8) {
      return 'The Password must be at least 8 characters.';
    }
    return null;
  }

  ///valida todo el registro y lo guarda en el estado
  Future submit() async {
    // First validate form.
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save(); // Save our form now.
      try {
        _presiono = true;
        // creates the firebase user
        FireBaseAuth auth = new FireBaseAuth();
        var user = await auth.createUser(_email, _password);
        //creates Map to add to firestore
        String nacimiento = _birthDay.day.toString() +
            '/' +
            _birthDay.month.toString() +
            '/' +
            _birthDay.year.toString();
        Map<String, dynamic> data = {
          'email': this._email,
          'firstName': this._nombre,
          'lastName': this._apellido,
          'sex': this._sexo,
          'birthDate': nacimiento,
          'skinTone': -1.0,
          'skinToneImage': '',
          'id': user.uid,
        };
        // adds it to the database.
        _services.addUser(data, user.uid);
        //success
        await showDialog(
                context: this.context,
                builder: (context) => approvedMessage(context, "Bienvenido!"))
            .then((value) {
          Navigator.pop(context);
        });
      } catch (e) {
        showDialog(
          context: this.context,
          builder: (context) => errorMessage(context, e.message),
        );
      }finally{
        _presiono = false;
      }
    }
  }

  /// selecciona el dia de nacimiento
  Future seleccioarDia(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _birthDay,
        firstDate: DateTime(1940, 1),
        lastDate: _birthDay);
    if (picked != null && picked != _birthDay)
      setState(() {
        _birthDay = picked;
      });
  }

  /// Maneja el estado del RadioBotton
  void handleRadioValueChange(int value) {
    setState(() {
      _sexo = value;
    });
  }

  ///crea el view del registro
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color.fromARGB(255, 50, 86, 170),
        child: ListView(
          children: <Widget>[
            Hero(
              tag: "intro",
              child: Container(
                constraints: BoxConstraints(maxWidth: 200, maxHeight: 200),
                child: Image(image: new AssetImage("assets/logo.png"),)
              ),
            ),
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 5,
              child: Column(
                children: <Widget>[
                  Text(
                    'Registrate',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 40, right: 40),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: 20,
                          ),
                          BuildTextFormField(
                              null, null, false, 'Nombre', 'Nombre',
                              (String value) {
                            this._nombre = value;
                          }, context),
                          SizedBox(
                            height: 20,
                          ),
                          BuildTextFormField(
                              null, null, false, 'Apellido', 'Apellido',
                              (String value) {
                            this._apellido = value;
                          }, context),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            "Sexo",
                            style: Theme.of(context).textTheme.subtitle,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Radio(
                                groupValue: _sexo,
                                value: 0,
                                onChanged: handleRadioValueChange,
                              ),
                              Text("Femenino"),
                              SizedBox(
                                width: 30,
                              ),
                              Radio(
                                groupValue: _sexo,
                                value: 1,
                                onChanged: handleRadioValueChange,
                              ),
                              Text("Masculino"),
                            ],
                          ),
                          Text(
                            "Fecha de Nacimiento",
                            style: Theme.of(context).textTheme.subtitle,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: <Widget>[
                              RaisedButton(
                                color: Color.fromARGB(255, 27, 154, 247),
                                onPressed: () => seleccioarDia(context),
                                child: Text("seleccionar..",
                                    style: TextStyle(
                                      color: Colors.white,
                                    )),
                              ),
                              SizedBox(
                                width: 50,
                              ),
                              Container(
                                child: (_birthDay == null)
                                    ? Text('Seleccionar')
                                    : Text(
                                        '${_birthDay.day}/${_birthDay.month}/${_birthDay.year}',
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          BuildTextFormField(null, this._validateEmail, false,
                              'you@mail.com', 'E-mail', (String value) {
                            this._email = value;
                          }, context),
                          SizedBox(
                            height: 20,
                          ),
                          BuildTextFormField(_textController, _validatePassword,
                              true, 'Contraseña', 'Contraseña', (String value) {
                            this._password = value;
                          }, context),
                          SizedBox(
                            height: 20,
                          ),
                          BuildTextFormField(null, (String value) {
                            if (this._textController.text != value) {
                              return 'Tiene que coincidir con la contraseña de arriba';
                            }
                          }, true, 'Contraseña', 'Repita la contraseña',
                              (String value) {}, context),
                          SizedBox(
                            height: 20,
                          ),
                          new RaisedButton(
                            color: Color.fromARGB(255, 27, 154, 247),
                            onPressed: !_presiono? submit: CircularProgressIndicator() ,
                            child: Text(
                              'Regístrarse',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

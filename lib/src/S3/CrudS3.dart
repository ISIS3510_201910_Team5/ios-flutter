import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:async/async.dart';
import 'package:http/http.dart' as http;
import 'package:amazon_cognito_identity_dart/sig_v4.dart';
import 'package:ios_flutter/src/S3/Signature.dart';

class CrudS3 {
  /// ejemplo rutaExterna 1234/skinToneImage/
  /// ejemplo nombreimagen: figura.jpg
  Future<String> send(String nombreImagen, String ruta, String rutaExterna) async {
    const _accessKeyId = 'AKIAILMHINXIQF46KSGQ';
    const _secretKeyId = 'WZkfqUY/h8Kb1O7N3C/4C+qUv23HY/d9dQb2BflY';
    const _region = 'sa-east-1';
    const _s3Endpoint = 'https://leskan-app.s3-sa-east-1.amazonaws.com';

    final file = File(ruta);
    final stream = http.ByteStream(DelegatingStream.typed(file.openRead()));
    final length = await file.length();

    final uri = Uri.parse(_s3Endpoint);
    final req = http.MultipartRequest("POST", uri);
    final multipartFile = http.MultipartFile('file', stream, length,
        filename: path.basename(file.path));

    final policy = Policy.fromS3PresignedPost(
        rutaExterna + nombreImagen , 'leskan-app', _accessKeyId, 15, length,
        region: _region);
    final key =
        SigV4.calculateSigningKey(_secretKeyId, policy.datetime, _region, 's3');
    final signature = SigV4.calculateSignature(key, policy.encode());

    req.files.add(multipartFile);
    req.fields['key'] = policy.key;
    req.fields['acl'] = 'public-read';
    req.fields['X-Amz-Credential'] = policy.credential;
    req.fields['X-Amz-Algorithm'] = 'AWS4-HMAC-SHA256';
    req.fields['X-Amz-Date'] = policy.datetime;
    req.fields['Policy'] = policy.encode();
    req.fields['X-Amz-Signature'] = signature;

    String resp = "https://s3-sa-east-1.amazonaws.com/leskan-app/" + rutaExterna + nombreImagen;

    try {
      final res = await req.send();
      await for (var value in res.stream.transform(utf8.decoder)) {
        print(value);
      }
    } catch (e) {
      print(e.toString());
    }

    return resp;
  }
}

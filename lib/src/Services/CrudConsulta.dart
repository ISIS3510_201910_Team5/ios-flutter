import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Modelo/Consulta.dart';
import 'package:ios_flutter/src/Modelo/Seguimiento.dart';

class CrudMethodsConsulta {
  addConsulta(Consulta consulta, String uid, Seguimiento seg) {
    var auth = new FireBaseAuth();
    if (auth.isLoggedin()) {
      String id = Firestore.instance
          .collection('USER')
          .document(uid)
          .collection("TRACING")
          .document(consulta.indicador)
          .collection("CONSULTATION")
          .document()
          .documentID;

      Map<String, dynamic> data = {
        'city': consulta.city,
        'country': consulta.country,
        'date': consulta.date,
        'image': consulta.image,
        'state': consulta.state,
        'id': id
      };

      Firestore.instance
          .collection('USER')
          .document(uid)
          .collection("TRACING")
          .document(consulta.indicador)
          .collection("CONSULTATION")
          .document(id)
          .setData(data);

      List imagenes = [];


      for (var i = 0; i < seg.consultas.length; i++) {
        imagenes.add(seg.consultas[i].image);
      }
      print(imagenes);

      Map<String, dynamic> data2 = {
        "user": uid,
        "urls": imagenes,
        "tracing": consulta.indicador,
        "consultation": id,
        "isSimulated": false,
      };
      CloudFunctions.instance
          .call(functionName: 'rapidResponseAnalizer', parameters: data2)
          .then((resp) {
          Firestore.instance
              .collection('USER')
              .document(uid)
              .collection("TRACING")
              .document(consulta.indicador)
              .get()
              .then((tracing) {
            seg.state = tracing.data['state'];
          });
      });
    }
  }
}

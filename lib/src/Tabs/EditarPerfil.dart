import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Login/errorMessage.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class EditarPerfil extends StatefulWidget {
  final Widget child;
  final User user;

  EditarPerfil(this.user, {Key key, this.child}) : super(key: key);

  _EditarState createState() => _EditarState(user);
}

class _EditarState extends State<EditarPerfil> {
  _EditarState(this._user);

  TextEditingController nameController = new TextEditingController();
  User _user;
  int _sexo;
  String _nombre;
  String _apellido;
  final _formNombre = GlobalKey<FormState>();
  int _lesionesFrecuentes;

  int _precedentesFamiliares;

  DateTime _birthDay = new DateTime.now();

  Future seleccioarDia(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _birthDay,
        firstDate: DateTime(1940, 1),
        lastDate: DateTime.now());

    if (picked != null && picked != _birthDay)
      setState(() {
        _birthDay = picked;
      });
  }

  void handleRadioValueChange(int value) {
    setState(() {
      _sexo = value;
    });
  }

  void initState() {
    setState(() {
      // _birthDay = DateTime.parse(_user.birthDay);

      _sexo = _user.sex;

      switch (_precedentesFamiliares) {
        case 0:
          _user.familySkinCancer = false;
          break;
        case 1:
          _user.familySkinCancer = true;
          break;
      }
      switch (_lesionesFrecuentes) {
        case 0:
          _user.commonIssue = false;
          break;
        case 1:
          _user.commonIssue = true;
          break;
      }
    });
    super.initState();
  }

  void manejoLesionesFrecuentes(int value) {
    setState(() {
      _lesionesFrecuentes = value;
    });
  }

  void manejoAntecedentes(int value) {
    setState(() {
      _precedentesFamiliares = value;
    });
  }

  void submit() async {
    var estado = await Connectivity().checkConnectivity();
    if (estado == ConnectivityResult.none) {
      showDialog(
        context: this.context,
        builder: (context) =>
            errorMessage(context, "No esta conectado a internet"),
      );
      return;
    }
    if (this._formNombre.currentState.validate()) {
      _formNombre.currentState.save();
      String nacimiento = _birthDay.day.toString() +
          '/' +
          _birthDay.month.toString() +
          '/' +
          _birthDay.year.toString();
      _user.name = this._nombre;
      _user.lastName = this._apellido;
      _user.sex = this._sexo;
      _user.birthDay = nacimiento;
      if (_precedentesFamiliares != null) {
        if (this._precedentesFamiliares == 0) {
          _user.familySkinCancer = true;
        } else {
          _user.familySkinCancer = false;
        }
      }

      if (_lesionesFrecuentes != null) {
        if (this._lesionesFrecuentes == 0) {
          _user.commonIssue = true;
        } else {
          _user.commonIssue = false;
        }
      }
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    FireBaseAuth _auth = new FireBaseAuth();

    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: Icon(FontAwesomeIcons.signOutAlt),
              onPressed: () {
                _auth.logOut();
                Navigator.popUntil(context, ModalRoute.withName('/'));
              },
            ),
          ],
          title: Text(
            'Editar Perfil',
            style: (Theme.of(context).textTheme.headline),
            textAlign: TextAlign.center,
          ),
          leading: Icon(
            Icons.accessibility,
            color: Theme.of(context).colorScheme.surface,
          ),
          backgroundColor: Color.fromARGB(255, 50, 86, 170)),
      body: Form(
        key: _formNombre,
        child: Container(
          color: Theme.of(context).colorScheme.onBackground,
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    "Nombre: ",
                    style: Theme.of(context).textTheme.subtitle,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    child: Flexible(
                      child: TextFormField(
                        onSaved: (value) {
                          _nombre = value;
                        },
                        validator: (value) {
                          if (value == null || value == "") {
                            return "No puede quedar vacio el campo.";
                          } else {
                            return (null);
                          }
                        },
                        textCapitalization: TextCapitalization.words,
                        initialValue: "${_user.name}",
                      ),
                    ), //flexible
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    "Apellido: ",
                    style: Theme.of(context).textTheme.subtitle,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    child: Flexible(
                      child: TextFormField(
                        onSaved: (value) {
                          _apellido = value;
                        },
                        validator: (value) {
                          if (value == null || value == "") {
                            return "No puede quedar vacio el campo.";
                          } else {
                            return (null);
                          }
                        },
                        textCapitalization: TextCapitalization.words,
                        initialValue: "${_user.lastName}",
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    "Sexo: ",
                    style: Theme.of(context).textTheme.subtitle,
                  ),
                  Radio(
                    groupValue: _sexo,
                    value: 0,
                    onChanged: handleRadioValueChange,
                  ),
                  Text("Femenino"),
                  SizedBox(
                    width: 30,
                  ),
                  Radio(
                    groupValue: _sexo,
                    value: 1,
                    onChanged: handleRadioValueChange,
                  ),
                  Text("Masculino"),
                ],
              ),
              SizedBox(height: 20),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  Text(
                    "Fecha de Nacimiento",
                    style: Theme.of(context).textTheme.subtitle,
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  RaisedButton(
                    onPressed: () => seleccioarDia(context),
                    child: Text("Cambiar Fecha"),
                  ),
                  SizedBox(
                    width: 50,
                  ),
                  Container(
                    child: (_birthDay == DateTime.now())
                        ? Text('${_user.birthDay}')
                        : Text(
                            '${_birthDay.day}/${_birthDay.month}/${_birthDay.year}',
                            style: Theme.of(context).textTheme.subtitle),
                  ),
                ],
              ),
              SizedBox(height: 20),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    "¿Presenta lesiones de piel frecuentemente?",
                    style: Theme.of(context).textTheme.subtitle,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Radio(
                    groupValue: _lesionesFrecuentes,
                    value: 0,
                    onChanged: manejoLesionesFrecuentes,
                  ),
                  Text("Sí"),
                  SizedBox(
                    width: 10,
                  ),
                  Radio(
                    groupValue: _lesionesFrecuentes,
                    value: 1,
                    onChanged: manejoLesionesFrecuentes,
                  ),
                  Text("No"),
                ],
              ),
              SizedBox(height: 20),
              Row(
                children: <Widget>[
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    "¿Algún familiar suyo ha sufrido de cáncer en la piel?",
                    style: Theme.of(context).textTheme.subtitle,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Radio(
                    groupValue: _precedentesFamiliares,
                    value: 0,
                    onChanged: manejoAntecedentes,
                  ),
                  Text("Sí"),
                  SizedBox(
                    width: 10,
                  ),
                  Radio(
                    groupValue: _precedentesFamiliares,
                    value: 1,
                    onChanged: manejoAntecedentes,
                  ),
                  Text("No"),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: RaisedButton(
                    child: Text("Guardar"),
                    color: Theme.of(context).accentColor,
                    elevation: 4,
                    onPressed: submit,
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

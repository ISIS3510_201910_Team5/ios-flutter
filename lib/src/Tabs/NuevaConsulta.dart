import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Camara/camara.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Modelo/Consulta.dart';
import 'package:ios_flutter/src/Modelo/Seguimiento.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:ios_flutter/src/S3/CrudS3.dart';
import 'package:ios_flutter/src/Services/CrudConsulta.dart';
import 'package:ios_flutter/src/Services/CrudSeguimiento.dart';
import 'package:ios_flutter/src/Widgets/BuildTextFormField.dart';
import 'package:pref_dessert/pref_dessert.dart';
import 'package:connectivity/connectivity.dart';
import 'package:image_picker/image_picker.dart';
import 'package:toast/toast.dart';

class NuevaConsulta extends StatefulWidget {
  final Widget child;
  final User user;

  NuevaConsulta(this.user, {Key key, this.child}) : super(key: key);

  _NuevaConsultaState createState() => _NuevaConsultaState(user);
}

class _NuevaConsultaState extends State<NuevaConsulta> {
  User _user;

  String _imageURL;

  final _formKey = GlobalKey<FormState>();

  String _titulo;

  bool _seg = false;

  _NuevaConsultaState(this._user);

  imagenDeGaleria(ImageSource source) async {
    File imageFile = await ImagePicker.pickImage(source: source);
    setState(() {
      _imageURL = imageFile.path;
    });
  }

  void guardar() async {
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save();
      if (_imageURL == null) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text("Coloque una imagen"),
        ));
      } else {
        var estado = await Connectivity().checkConnectivity();
        if (estado == ConnectivityResult.none) {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text("No esta conectado a internet"),
          ));
          return;
        }
        try {
          _seg = true;
          Seguimiento seg = new Seguimiento();
          Consulta cons = new Consulta();
          var auth = new FireBaseAuth();

          var userAuth = await auth.getUser();

          var lista = _user.seguimientos;

          lista?.forEach((segimiento) {
            if (segimiento.name == _titulo) {
              Scaffold.of(context).showSnackBar(SnackBar(
                content: Text("Ya hay un seguimiento con ese nombre"),
              ));
              return;
            }
          });

          CrudS3 s3 = new CrudS3();
          var date = DateTime.now();
          var dateString = date.day.toString() +
              "/" +
              date.month.toString() +
              "/" +
              date.year.toString() +
              " - " +
              date.hour.toString() +
              ":" +
              date.minute.toString() +
              ":" +
              date.second.toString();

          Toast.show(
            "Enviando imagen.", context,
          );
          String tituloSinEspacios =
              _titulo.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
          String imageURL = await s3
              .send(date.toString() + '.jpg', _imageURL,
                  userAuth.uid + '/tracings/' + tituloSinEspacios + '/')
              .timeout(const Duration(seconds: 10), onTimeout: _onTimeout);

          // agrego al modelo
          seg.name = _titulo;
          seg.colorChange = "";
          seg.state = "LOADING";
          seg.textureChange = "";
          seg.sizeChange = "";
          seg.date = dateString;

          cons.city = '';
          cons.country = '';

          cons.date = dateString;
          cons.image = imageURL;
          cons.indicador = _titulo;
          cons.state = 'LOADING';
          cons.city = "";
          cons.country = "";

          Toast.show(
            "Imagen enviada.", context,
          );

          seg.addConsulta(cons);

          _user.addSeguimiento(seg);

          //persisto localmente
          var repoCons =
              new FuturePreferencesRepository<Consulta>(new ConsultaDesSer());
          var repoSeg = new FuturePreferencesRepository<Seguimiento>(
              new SeguimientoDesSer());
          repoCons.save(cons);
          repoSeg.save(seg);

          // guardo en base de datos
          CrudMethodsSeguimiento crudSeg = new CrudMethodsSeguimiento();
          CrudMethodsConsulta crudCons = new CrudMethodsConsulta();

          crudSeg.addSeguimiento(seg, userAuth.uid);
          crudCons.addConsulta(cons, userAuth.uid,seg);

          Toast.show(
            "Procesando la imagen.", context,
          );
          

          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text("se anadio la consulta"),
          ));
          _seg = false;
        } catch (e) {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(e.toString()),
          ));
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
          backgroundColor: Color.fromARGB(255, 50, 86, 170),
          automaticallyImplyLeading: false,
          title: Text(
            "Generar una nueva consulta",
            textAlign: TextAlign.center,
          )),
      body: ListView(
        padding: EdgeInsets.all(40),
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          (_imageURL == null)
              ? Container(
                  color: Color.fromARGB(255, 50, 86, 170),
                  child: SizedBox(
                    child: Icon(
                      Icons.camera_alt,
                      size: 50,
                      color: Colors.white,
                    ),
                    width: 200.0,
                    height: 200.0,
                  ),
                )
              : SizedBox(
                  child: Image.file(File(_imageURL)),
                ),
          SizedBox(
            height: 20,
          ),
          Text(
            "Toma una foto de la lesión en la piel presentas",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 12),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              RaisedButton(
                child: Text(
                  "Galeria",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                color: Color.fromARGB(255, 50, 86, 170),
                onPressed: () {
                  imagenDeGaleria(ImageSource.gallery);
                },
              ),
              RaisedButton(
                child: Text(
                  "Camara",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                color: Color.fromARGB(255, 50, 86, 170),
                onPressed: () async {
                  var image = await Navigator.push(
                      context,
                      MaterialPageRoute<String>(
                        builder: (context) => Camara(),
                      ));
                  setState(() {
                    _imageURL = image;
                  });
                },
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                BuildTextFormField(
                    null,
                    (value) {
                      if (value == null || value.length < 5) {
                        return "El titulo debe ser de al menos 5 caracteres";
                      }
                      if (value.contains('.') ||
                          value.contains('/') ||
                          value.contains('\\') ||
                          value.contains('|') ||
                          value.contains(',')) {
                        return "El no puede contener ningun signo \n de puntuacion diferentes a caracteres";
                      }
                      return null;
                    },
                    false,
                    "Mi seguimiento",
                    "Titulo",
                    (String value) {
                      setState(() {
                        this._titulo = value;
                      });
                    },
                    context),(!_seg)?
                RaisedButton(
                  color: Color.fromARGB(255, 50, 86, 170),
                  onPressed: (){
                    guardar();
                    },
                  child: Text(
                    "Confirmar",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ):CircularProgressIndicator(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  FutureOr<String> _onTimeout() {
    throw new Exception("No se pudo enviar la imagen. Intente de nuevo");
  }
}

import 'package:flutter/material.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ios_flutter/src/Tabs/EditarPerfil.dart';

class Profile extends StatefulWidget {
  final Widget child;
  final User user;

  Profile(this.user, {Key key, this.child}) : super(key: key);

  _ProfileState createState() => _ProfileState(user);
}

class _ProfileState extends State<Profile> {
  _ProfileState(this._user);

  User _user;

  @override
  Widget build(BuildContext context) {
    FireBaseAuth _auth = new FireBaseAuth();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 50, 86, 170),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(FontAwesomeIcons.signOutAlt),
            onPressed: () {
              _auth.logOut();
              Navigator.popUntil(context, ModalRoute.withName('/'));
            },
          ),
        ],
        title: Text(
          'Tu Perfil',
          textAlign: TextAlign.center,
        ),
        automaticallyImplyLeading: false,
      ),
      body: Container(
        color: Theme.of(context).colorScheme.onBackground,
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Center(
              child: Text("${_user.name} ${_user.lastName}",
                  style: Theme.of(context).textTheme.display1),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Text("${_user.email}",
                  style: Theme.of(context).textTheme.title),
            ),
            SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 80),
                Icon((_user.sex == 1)
                    ? FontAwesomeIcons.male
                    : FontAwesomeIcons.female),
                SizedBox(width: 60),
                Text((_user.sex == 1) ? "Masculino" : "Femenino",
                    style: Theme.of(context).textTheme.title),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 80),
                Icon(FontAwesomeIcons.birthdayCake),
                SizedBox(width: 60),
                Text("${_user.birthDay}",
                    style: Theme.of(context).textTheme.title),
              ],
            ),
            SizedBox(
              height: 40,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("¿Presenta lesiones en la piel",
                    style: Theme.of(context).textTheme.title),
                Text("frecuentemente?",
                    style: Theme.of(context).textTheme.title)
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Center(
              child: Text((_user.commonIssue) ? "Sí" : "No",
                  style: Theme.of(context).textTheme.headline),
            ),
            SizedBox(
              height: 30,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("¿Algún familiar suyo ha sufrido de",
                    style: Theme.of(context).textTheme.title),
                Text("cáncer en la piel?",
                    style: Theme.of(context).textTheme.title)
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Center(
              child: Text((_user.familySkinCancer) ? "Sí" : "No",
                  style: Theme.of(context).textTheme.headline),
            ),
            SizedBox(
              height: 20,
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: IconButton(
                icon: Icon(FontAwesomeIcons.pen),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => EditarPerfil(_user)),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

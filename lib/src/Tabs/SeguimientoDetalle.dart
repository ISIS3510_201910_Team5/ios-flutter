import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Camara/camara.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Modelo/Consulta.dart';
import 'package:ios_flutter/src/Modelo/Seguimiento.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:ios_flutter/src/S3/CrudS3.dart';
import 'package:ios_flutter/src/Services/CrudConsulta.dart';
import 'package:pref_dessert/pref_dessert.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:toast/toast.dart';

class SeguimientoDetalle extends StatefulWidget {
  final Widget child;
  final User user;
  final Seguimiento segimiento;

  SeguimientoDetalle(this.user, this.segimiento, {Key key, this.child})
      : super(key: key);

  _SeguimientoDetalleState createState() =>
      _SeguimientoDetalleState(user, segimiento);
}

class _SeguimientoDetalleState extends State<SeguimientoDetalle> {
  User _user;
  Seguimiento _segimiento;

  _SeguimientoDetalleState(this._user, this._segimiento);

  @override
  Widget build(BuildContext context) {
    void addConsulta() async {
      var image = await Navigator.push(
          context,
          MaterialPageRoute<String>(
            builder: (context) => Camara(),
          ));

      if (image == null) {
        return;
      }

      var estado = await Connectivity().checkConnectivity();
      if (estado == ConnectivityResult.none) {
        Toast.show(
              "No esta conectado a internet, revise su conexión e intente nuevamente.", 
              context,
          duration: Toast.LENGTH_LONG,
          gravity:  Toast.BOTTOM
        );
        return;
      }

      Consulta nva = new Consulta();

      var auth = new FireBaseAuth();

      var userAuth = await auth.getUser();

      // envio imagen a S3
      CrudS3 s3 = new CrudS3();
      var date = DateTime.now();
      var dateString = date.day.toString() +
          "/" +
          date.month.toString() +
          "/" +
          date.year.toString() +
          " - " +
          date.hour.toString() +
          ":" +
          date.minute.toString() +
          ":" +
          date.second.toString();

      String tituloSinEspacios =
          _segimiento.name.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
      String imageURL = await s3.send(dateString + '.jpg', image,
          userAuth.uid + '/tracings/' + tituloSinEspacios + '/');
      nva.date = dateString;
      nva.image = imageURL;
      nva.indicador = _segimiento.name;
      nva.state = "LOADING";
      nva.country = "";
      nva.city = "";
      setState(() {
        _segimiento.addConsulta(nva);
      });

      //Persisto
      var repoCons =
          new FuturePreferencesRepository<Consulta>(new ConsultaDesSer());
      repoCons.save(nva);

      //Envio a firestore
      CrudMethodsConsulta crudCons = new CrudMethodsConsulta();
      crudCons.addConsulta(nva, userAuth.uid,_segimiento);
    }

    var dia = _segimiento.date;
    if (_segimiento.colorChange == null || _segimiento.colorChange == "") {
      _segimiento.colorChange = "0";
    }
    if (_segimiento.sizeChange == null || _segimiento.sizeChange == "") {
      _segimiento.sizeChange = "0";
    }
    if (_segimiento.textureChange == null || _segimiento.textureChange == "") {
      _segimiento.textureChange = "0";
    }
    String fecha = _segimiento.date;

    var state;
    var color = 0xFFFFFFFF;

    if (_segimiento.state == "LOADING") {
      state = "CARGANDO";
      color = 0xFFFFBF60;
    } else if (_segimiento.state == "DANGER") {
      state = "PELIGRO";
      color = 0xFFB00020;
    } else if (_segimiento.state == "NO_DANGER") {
      state = "NO ES PELIGROSO";
      color = 0xFF43A047;
    } else if (_segimiento.state == "POTENTIALLY_DANGER") {
      state = "POTENCIALMENTE PELIGROSO";
      color = 0xFFFF6700;
    } else {
      state = "ESTADO NO RECONOCIDO";
    }

    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Color.fromARGB(255, 50, 86, 170),
        title: Text(
          'Detalles',
          textAlign: TextAlign.center,
        ),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverFixedExtentList(
              itemExtent: 520,
              delegate: SliverChildListDelegate([
                Container(
                  child: Column(
                    children: <Widget>[
                      Hero(
                        child: Container(
                          margin: EdgeInsets.all(10),
                          child: Container(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  _segimiento.name,
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context).textTheme.title,
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  fecha,
                                  textAlign: TextAlign.left,
                                  style: Theme.of(context).textTheme.subtitle,
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10.0)),
                                    color: Color(color),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 7, vertical: 3),
                                    child: Text(
                                      state,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        tag: _segimiento.name,
                      ),
                      new Container(
                        height: 2,
                        color: Color.fromARGB(255, 50, 86, 170),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 20, right: 20, bottom: 20, top: 20),
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  "Crecimiento",
                                  style: Theme.of(context).textTheme.title,
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  Text(
                                    _segimiento.sizeChange.toString() + '%',
                                    style: Theme.of(context).textTheme.title,
                                  ),
                                  Flexible(
                                    child: Text(
                                      "Aumento del ${_segimiento.sizeChange} % en el tamaño respecto a la primer consulta.",
                                      overflow: TextOverflow.clip,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      new Container(
                        height: 2,
                        color: Color.fromARGB(255, 50, 86, 170),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.all(20),
                              child: Container(
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      child: Text(
                                        "Color",
                                        style:
                                            Theme.of(context).textTheme.title,
                                      ),
                                      padding: EdgeInsets.only(bottom: 10),
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Text(
                                          _segimiento.colorChange + '%',
                                          style:
                                              Theme.of(context).textTheme.title,
                                        ),
                                        Flexible(
                                          child: Text(
                                            "variacion del ${_segimiento.colorChange} % en el color respecto a la primera consulta.",
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          new Container(
                            width: 2,
                            height: 130,
                            padding: EdgeInsets.all(10),
                            color: Color.fromARGB(255, 50, 86, 170),
                          ),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.all(10),
                              child: Container(
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                      child: Text(
                                        "Textura",
                                        style:
                                            Theme.of(context).textTheme.title,
                                      ),
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Text(
                                          _segimiento.textureChange + '%',
                                          style:
                                              Theme.of(context).textTheme.title,
                                        ),
                                        Flexible(
                                          child: Text(
                                            "Cambio del ${_segimiento.textureChange} % en la forma respecto a la primera consulta.",
                                            overflow: TextOverflow.clip,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      new Container(
                        height: 2,
                        color: Color.fromARGB(255, 50, 86, 170),
                      ),
                      Container(
                        margin:
                            EdgeInsets.only(left: 10, right: 20, bottom: 20),
                        child: Padding(
                          padding: EdgeInsets.all(15),
                          child: Text(
                            "Consultas Realizadas",
                            style: Theme.of(context).textTheme.display1,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ])),
          SliverList(
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
              var consulta = _segimiento.consultas[index];
              String fecha = consulta.date;
              return Container(
                  margin: EdgeInsets.only(left: 10, right: 20, bottom: 20),
                  decoration: new BoxDecoration(
                    border: new Border.all(
                        width: 2, color: Color.fromARGB(255, 50, 86, 170)),
                  ),
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        child: CachedNetworkImage(
                          imageUrl: consulta.image,
                          fit: BoxFit.scaleDown,
                          placeholder: (context, url) =>
                              new CircularProgressIndicator(),
                          height: 100,
                          width: 100,
                        ),
                        onTap:(){
                          showDialog(
                        context: this.context,
                        builder: (context) => ImageDialog(
                            context, consulta.image),
                      );
                        } ,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        fecha,
                        style: Theme.of(context).textTheme.title,
                      ),
                    ],
                  ));
            }, childCount: _segimiento.consultas.length),
          ),
          SliverFixedExtentList(
            itemExtent: 50,
            delegate: SliverChildListDelegate([
              GestureDetector(
                onTap: addConsulta,
                child: Container(
                  margin: EdgeInsets.only(left: 10, right: 20, bottom: 20),
                  decoration: new BoxDecoration(
                    border: new Border.all(
                        width: 2, color: Color.fromARGB(255, 50, 86, 170)),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.add),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        "Agregar Consulta",
                        style: Theme.of(context).textTheme.button,
                      )
                    ],
                  ),
                ),
              )
            ]),
          )
        ],
      ),
    );
  }
}

Widget ImageDialog(BuildContext context, String image) {
  return SimpleDialog(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.zero,
        child: CachedNetworkImage(imageUrl: image,
        fit: BoxFit.fill,)
      )
    ],
  );
}

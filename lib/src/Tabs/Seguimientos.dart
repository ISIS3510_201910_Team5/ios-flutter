import 'package:flutter/material.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:ios_flutter/src/Services/CrudSeguimiento.dart';
import 'package:ios_flutter/src/Tabs/SeguimientoDetalle.dart';

class Seguimientos extends StatefulWidget {
  final Widget child;
  final User user;

  Seguimientos(this.user, {Key key, this.child}) : super(key: key);

  _SeguimientosState createState() => _SeguimientosState(user);
}

class _SeguimientosState extends State<Seguimientos> {
  User _user;
  _SeguimientosState(this._user);

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          backgroundColor: Color.fromARGB(255, 50, 86, 170),
          title: Text(
            'Tus Seguimientos',
            textAlign: TextAlign.center,
          ),
          automaticallyImplyLeading: false,
        ),
        body: (_user.seguimientos.length == 0)
            ? Column(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Todavia no tienes seguimientos.",
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.display1,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Dirigente a la pestaña de Nueva consulta para agregar una nueva.",
                    textAlign: TextAlign.center,
                  )
                ],
              )
            : RefreshIndicator(
                onRefresh: () async {
                  CrudMethodsSeguimiento metodos = new CrudMethodsSeguimiento();
                  var auth = new FireBaseAuth();
                  var userAuth = await auth.getUser();
                  var user = await metodos.getSeguimientos(userAuth.uid, _user);
                  setState(() {
                    _user = user;
                  });
                  return;
                },
                child: ListView.builder(
                  itemCount: _user.seguimientos.length,
                  padding: EdgeInsets.only(bottom: 5),
                  itemBuilder: (BuildContext context, int index) {
                    var segimiento = _user.seguimientos[index];
                    var dia = segimiento.date;
                    String fecha = dia;
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute<String>(
                              builder: (context) =>
                                  SeguimientoDetalle(_user, segimiento),
                            ));
                      },
                      child: Hero(
                        child: Container(
                          decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: new Border.all(
                                width: 2,
                                color: Color.fromARGB(255, 50, 86, 170)),
                          ),
                          margin: EdgeInsets.all(20),
                          child: Container(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  segimiento.name,
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context).textTheme.title,
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      fecha,
                                      textAlign: TextAlign.left,
                                      style:
                                          Theme.of(context).textTheme.subtitle,
                                    ),
                                    Expanded(
                                      child: Container(),
                                    ),
                                    (segimiento.state == 'LOADING')
                                        ? Icon(
                                            Icons.access_time,
                                            color: Colors.yellow,
                                          )
                                        : Icon(
                                            Icons.info_outline,
                                            color: Colors.blue,
                                          )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        tag: segimiento.name,
                      ),
                    );
                  },
                ),
              ));
  }
}

import 'package:flutter/material.dart';

class MyTheme extends Theme {
  final Widget child;

  MyTheme({Key key, this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: child,
    );
  }
}
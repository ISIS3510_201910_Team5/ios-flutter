import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:ios_flutter/src/Tabs/Seguimientos.dart';
import 'package:ios_flutter/src/Tabs/Profile.dart';
import 'package:ios_flutter/src/Tabs/NuevaConsulta.dart';


class FirstPage extends StatefulWidget {
  final Widget child;

  final User user;

  FirstPage(this.user, {Key key, this.child}) : super(key: key);

  _FirstPageState createState() => _FirstPageState(user);
}

class _FirstPageState extends State<FirstPage> {
  _FirstPageState(this._user);

  User _user;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 3,
      child: new Scaffold(
        body: TabBarView(
          children: [
            new Seguimientos(_user),
            new NuevaConsulta(_user),
            new Profile(_user),
          ],
        ),
        bottomNavigationBar: new Material(
          color: Color.fromARGB(255, 50, 86, 170),
                  child: new TabBar(
            tabs: [
              Tab(
                text: "Seguimientos",
                icon: new Icon(Icons.description),
              ),
              Tab(
                text: "Nueva consulta",
                icon: new Icon(Icons.photo_camera),
              ),
              Tab(
                text: "Tu perfil",
                icon: new Icon(Icons.person),
              ),
            ],
            labelColor: Colors.white,
            unselectedLabelColor: Colors.white,
            indicatorSize: TabBarIndicatorSize.tab,
            indicatorPadding: EdgeInsets.all(5.0),
            indicatorColor: Colors.white,
          ),
        ),
        backgroundColor: Colors.blue,
      ),
    );
  }
}

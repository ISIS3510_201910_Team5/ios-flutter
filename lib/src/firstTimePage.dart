import 'dart:io';

import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ios_flutter/src/Camara/camara.dart';
import 'package:ios_flutter/src/FireBase/firebaseAuth.dart';
import 'package:ios_flutter/src/Modelo/User.dart';
import 'package:ios_flutter/src/S3/CrudS3.dart';
import 'package:ios_flutter/src/Services/CrudUser.dart';
import 'package:ios_flutter/src/Login/warningMessage.dart';
import 'package:ios_flutter/src/firstPage.dart';
import 'package:pref_dessert/pref_dessert.dart';

class FirstTimePage extends StatefulWidget {
  final Widget child;

  final User user;

  FirstTimePage(this.user, {Key key, this.child}) : super(key: key);

  _FirstTimePage createState() => _FirstTimePage(user);
}

class _FirstTimePage extends State<FirstTimePage> {
  _FirstTimePage(this._user);

  User _user;

  FireBaseAuth _auth = new FireBaseAuth();

  final _formKey = GlobalKey<FormState>();

  /// Maneja el estado de boton de la pregunta de lesiones frecuentes
  int _radioButtonState1;

  /// Maneja el estado de boton de la pregunta de antecedentes familiares
  int _radioButtonState2;

  /// True en el caso de que si sean lesiones frecuentes, false de lo contrario
  bool _lesionesFrecuentes;

  ///True en el caso de que si tenga antecedentes familiares.
  bool _presedentesFamiliares;

  String _imageURL;

  void manejoLesionesFrecuentes(int estado) {
    setState(() {
      _radioButtonState1 = estado;
    });
    switch (estado) {
      case 0:
        _lesionesFrecuentes = true;
        break;
      case 1:
        _lesionesFrecuentes = false;
        break;
    }
  }

  void manejoAntecedentes(int estado) {
    setState(() {
      _radioButtonState2 = estado;
    });
    switch (estado) {
      case 0:
        _presedentesFamiliares = true;
        break;
      case 1:
        _presedentesFamiliares = false;
        break;
    }
  }

  void guardar() async {
    CrudMethodsUser crud = new CrudMethodsUser();
    FirebaseUser miUser = await _auth.getUser();
    if (_lesionesFrecuentes == null) {
      showDialog(
        builder: ((context) => warningMessage(
            context, "Por favor llene el campo de lesiones frecuentes")),
        context: context,
      );
      return;
    }
    if (_presedentesFamiliares == null) {
      showDialog(
        builder: ((context) => warningMessage(
            context, "Por favor llene el campo de antecedentes familiares")),
        context: context,
      );
      return;
    }
    if (_imageURL == null) {
      showDialog(
        builder: ((context) =>
            warningMessage(context, "Por favor tome una foto de su piel")),
        context: context,
      );
      return;
    }
    _user.skinToneImage = _imageURL;
    _user.familySkinCancer = _presedentesFamiliares;
    _user.commonIssue = _lesionesFrecuentes;
    var repo = new FuturePreferencesRepository<User>(new UserDesSer());
    repo.removeAll();
    repo.save(_user);

    CrudS3 aws = new CrudS3();
    String rutaInterna = miUser.uid + '/' + "skinToneImage/";
    String res = await aws.send(miUser.uid + ".jpg", _imageURL, rutaInterna);
    await crud.addHistory(
        _lesionesFrecuentes, _presedentesFamiliares, res, miUser.uid);

        Map<String, dynamic> data = {"user": miUser.uid, "url": res};
          CloudFunctions.instance
              .call(functionName: 'rapidResponseASC', parameters: data)
              .then((resp) {
          });
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => FirstPage(_user)),
        ModalRoute.withName('/'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color.fromARGB(255, 50, 86, 170),
        child: ListView(
          children: <Widget>[
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 5,
              child: Container(
                padding: EdgeInsets.only(left: 40, right: 40),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Hola " + _user.name + ",",
                      style: Theme.of(context).textTheme.display1,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Para obtener resultados mas precisos, queremos hacerte unas preguntas más:",
                      style: Theme.of(context).textTheme.subhead,
                      textAlign: TextAlign.center,
                    ),
                    Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 40,
                          ),
                          Text(
                            "¿Presentas lesiones en la piel frecuentemente?",
                            style: Theme.of(context).textTheme.subtitle,
                            textAlign: TextAlign.left,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Radio(
                                groupValue: _radioButtonState1,
                                value: 0,
                                onChanged: manejoLesionesFrecuentes,
                              ),
                              Text("Sí"),
                              SizedBox(
                                width: 30,
                              ),
                              Radio(
                                groupValue: _radioButtonState1,
                                value: 1,
                                onChanged: manejoLesionesFrecuentes,
                              ),
                              Text("No"),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            "¿Algún familiar tuyo ha sufrido de cáncer en la piel?",
                            style: Theme.of(context).textTheme.subtitle,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Radio(
                                groupValue: _radioButtonState2,
                                value: 0,
                                onChanged: manejoAntecedentes,
                              ),
                              Text("Sí"),
                              SizedBox(
                                width: 30,
                              ),
                              Radio(
                                groupValue: _radioButtonState2,
                                value: 1,
                                onChanged: manejoAntecedentes,
                              ),
                              Text("No"),
                            ],
                          ),
                          Text(
                            "Sube una foto de la zona de tu piel que consideras representa de mejor manera tu tono natural. Trata de que la zona de la piel cubra toda la camara.",
                            style: Theme.of(context).textTheme.subtitle,
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          (_imageURL == null)
                              ? Text("")
                              : SizedBox(
                                  child: Image.file(File(_imageURL)),
                                  width: 128.0,
                                  height: 128.0,
                                ),
                          GestureDetector(
                            child: Icon(
                              Icons.photo_camera,
                              size: 50,
                            ),
                            onTap: () async {
                              _imageURL = await Navigator.push(
                                  context,
                                  MaterialPageRoute<String>(
                                    builder: (context) => Camara(),
                                  ));
                            },
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          RaisedButton(
                            child: Text('Guardar'),
                            onPressed: guardar,
                          )
                        ],
                      ),
                    ),
                    RaisedButton(
                      child: Text('log out'),
                      onPressed: () {
                        _auth.logOut();
                        Navigator.popUntil(context, ModalRoute.withName('/'));
                      },
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
